﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.Windows.Forms;
using ZzukBot;
using ZzukBot.ExtensionFramework.Interfaces;
using System.Runtime.InteropServices;
using System.IO;
using System.Media;

namespace WhisperBeep
{
    [Export(typeof(IPlugin))]
    public class WhisperBeep : IPlugin
    {
        public string Author
        {
            get
            {
                return "Rexas";
            }
        }

        public string Name
        {
            get
            {
                return "WhisperBeep";
            }
        }

        public int Version
        {
            get
            {
                return 1;
            }
        }

        public void Dispose()
        {

        }

        public bool Load()
        {
            ZzukBot.Game.Statics.WoWEventHandler.Instance.OnChatMessage += OnChatMessage;
            return true;
        }

        private async void OnChatMessage(object sender, ZzukBot.Game.Statics.WoWEventHandler.ChatMessageArgs e)
        {
            if (e.ChatChannel.Equals("Whisper"))
            {
                if (File.Exists("WhisperBeep.wav"))
                {
                    new SoundPlayer(@"WhisperBeep.wav").Play();
                }
                else
                {
                    for (int i = 0; i < 5; i++)
                    {
                        System.Media.SystemSounds.Exclamation.Play();
                        await Task.Delay(100);
                    }
                }
            }
        }

        public void ShowGui()
        {

        }

        public void Unload()
        {
            ZzukBot.Game.Statics.WoWEventHandler.Instance.OnChatMessage -= OnChatMessage;
        }
    }
}
